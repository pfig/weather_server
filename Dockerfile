# Define python3 as the base Image to be used in the build process.
FROM python:3

# Copy my application code into a docker container.
COPY ./app /app

# Set /app folder as my working directory.
WORKDIR /app

# Set the environment variables of the container.
ARG COUNTRY_FILE
ENV COUNTRY_FILE=${COUNTRY_FILE}
ARG API_KEY
ENV API_KEY=${API_KEY}
ARG HOST
ENV HOST=${HOST}

# Copy the requirements file to the Docker Image and installs the libraries it contains.
COPY requirements.txt .
RUN pip3 install -r requirements.txt

# Define the application used when a container is instantiated from the Image.
ENTRYPOINT ["python3"]
CMD ["main.py"]

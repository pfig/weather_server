import os
import requests

# Class used to create instances of the weather values for a given city.
# Contains the attributes 'city' and 'key' needed to send requests to the weather API.
# Contains also two methods responsible for getting the weather values for a given city.

class Bot():

    def __init__(self, city):
        self.city = city
        self.key = os.environ['API_KEY']

    # Returns the weather values within the json response.
    def parse(self, response):
        return response['main']

    # Gets the response from the weather API in json format.
    def get(self):
        try:
        	r = requests.get(f'http://api.openweathermap.org/data/2.5/weather?q={self.city}&units=metric&appid={self.key}')
        	if r.status_code == 200:
        		response = r.json()
        		return self.parse(response)
        	else:
        		print(f'- failed')
        		print(f'error code: {r.status_code}')
        except Exception as e:
        	print('- exception')
        	print(f'traceback: {e}')

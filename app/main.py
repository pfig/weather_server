#!flask/bin/python
from flask import Flask, jsonify, abort
from bot.bot import Bot
from collections import defaultdict
import os
import yaml

# After the import of the Flask class from the flask library, the app variable
# is assigned to an instance of the Flask class.
app = Flask(__name__)

# Reads the 'config.yaml' file, returns a dictionary and assigns it to 'countries'.
def converter(config_file):
	with open(config_file) as file:
	    country_dict = yaml.load(file, Loader=yaml.FullLoader)
	return country_dict

countries = converter(f'./config/{os.environ["COUNTRY_FILE"]}')

# Runs through 'countries' to create a dictionary of 'Bot' instances.
countries_dict = defaultdict(dict)
for country in countries:
    for city in countries[country]:
        countries_dict[country][city] = Bot(city)

# Shows the weather values for the cities of every country in 'countries'.
@app.route('/show_all/')
def show():
	for country in countries_dict:
		for city in countries_dict[country]:
			countries_dict[country][city] = Bot(city).get()
	return jsonify({f'countries': countries_dict})

# Shows the weather values for the cities within a specific country.
@app.route('/show/<country>/')
def show_country(country):
	if country in countries_dict:
		for city in countries_dict[country]:
			countries_dict[country][city] = Bot(city).get()
		return jsonify({f'{country}': countries_dict[country]})
	else:
		abort(404)

# Shows the weather values for a specific city within a country.
@app.route('/show/<country>/<city>/')
def show_city(country, city):
	if country in countries_dict and city in countries_dict[country]:
		countries_dict[country][city] = Bot(city).get()
		return jsonify({f'{country}': {f'{city}': countries_dict[country].get(city)}})
	else:
		abort(404)

# Runs the flask app when running 'python3 main.py'.
if __name__ == '__main__':
    app.run(debug=True, host=f'{os.environ["HOST"]}')

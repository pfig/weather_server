# Run an instance of the Docker Image.
run:
	docker run -d -p 5000:5000 weather_server

# Build the Docker Image.
build-image:
	./build

# Set the environment variables on the Host environment.
set-envs:
	./envs
